//
//  ProjectUI.h
//  ProjectUI
//
//  Created by Sander Rõuk on 07.06.2020.
//  Copyright © 2020 Sander Rõuk. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ProjectUI.
FOUNDATION_EXPORT double ProjectUIVersionNumber;

//! Project version string for ProjectUI.
FOUNDATION_EXPORT const unsigned char ProjectUIVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ProjectUI/PublicHeader.h>


