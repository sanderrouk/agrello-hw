import ProjectCrypto
import ProjectUI
import UIKit

public protocol MessageStorageFactory {
    func make() -> Coordinator
}

public struct MessageStorageFactoryImplementation: MessageStorageFactory {
    private let secureEnclave: SecureEnclave

    public init(secureEnclave: SecureEnclave) {
        self.secureEnclave = secureEnclave
    }

    public func make() -> Coordinator {
        let navigationController = UINavigationController()

        let coordinator = MessageStorageCoordinator(
            navigationController: navigationController,
            makeContentViewController: makeContentViewController
        )

        return coordinator
    }

    private func makeContentViewController() -> UIViewController {
        let viewModel = MessageStorageViewModel(secureEnclave: secureEnclave)
        let viewController = MessageStorageController(viewModel: viewModel)

        return viewController
    }
}
