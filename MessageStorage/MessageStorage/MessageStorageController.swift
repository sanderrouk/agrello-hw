import ProjectUI
import Resources
import UIKit

final class MessageStorageController: UIViewController {
    private let scrollView = UIScrollView()
    private let stackView = UIStackView()

    private let messageLabel = UILabel()
    private let messageField = UITextField()

    private let buttonsStack = UIStackView()
    private let encryptButton = UIButton(type: .system)
    private let decryptButton = UIButton(type: .system)
    private let resetButton = UIButton(type: .system)

    private let publicKeyLabel = UILabel()
    private let publicKeyValueLabel = UILabel()

    private let encryptedData = UILabel()
    private let encryptedDataLabel = UILabel()

    private let decryptedData = UILabel()
    private let decryptedDataLabel = UILabel()

    private let viewModel: MessageStorageViewModel

    init(viewModel: MessageStorageViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        setup()
        layout()
    }

    deinit {
        viewModel.unsubscribe(self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.subscribe(self)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }

    private func setup() {
        title = L10n.appTitle
        view.backgroundColor = .white

        scrollView.contentInset = .top(15)

        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 5

        setupLabelsAndFields()
        setupButtons()

        publicKeyLabel.text = L10n.publicKey
        publicKeyValueLabel.numberOfLines = 5
        publicKeyValueLabel.adjustsFontSizeToFitWidth = true

        encryptedData.text = L10n.encryptedData
        encryptedDataLabel.numberOfLines = 5
        encryptedDataLabel.adjustsFontSizeToFitWidth = true

        decryptedData.text = L10n.decryptedData
    }

    private func layout() {
        view.addSubview(scrollView)
        scrollView.pinEdgesToSuperviewKeyboardEdges()

        scrollView.addSubview(stackView)
        stackView.pinEdgesToSuperviewEdges()

        layoutLabelsAndFields()
        layoutButtons()

        stackView.addArrangedSubview(publicKeyLabel)
        publicKeyLabel.pinEdges(toSuperviewEdges: [.left, .right], insets: .horizontal(15))
        stackView.addArrangedSubview(publicKeyValueLabel)
        publicKeyValueLabel.matchDimension(.width, to: view)

        stackView.setCustomSpacing(30, after: publicKeyValueLabel)
        stackView.addArrangedSubview(encryptedData)
        encryptedData.pinEdges(toSuperviewEdges: [.left, .right], insets: .horizontal(15))
        stackView.addArrangedSubview(encryptedDataLabel)
        encryptedDataLabel.matchDimension(.width, to: view)

        stackView.setCustomSpacing(30, after: encryptedDataLabel)
        stackView.addArrangedSubview(decryptedData)
        decryptedData.pinEdges(toSuperviewEdges: [.left, .right], insets: .horizontal(15))
        stackView.addArrangedSubview(decryptedDataLabel)
        decryptedDataLabel.matchDimension(.width, to: view)
    }

    private func setupLabelsAndFields() {
        messageLabel.text = L10n.enterMessage
        messageLabel.textAlignment = .left

        messageField.placeholder = L10n.messagePlaceholder
        messageField.layer.borderWidth = 0.1
        messageField.layer.borderColor = UIColor.black.cgColor
        messageField.delegate = self
    }

    private func setupButtons() {
        buttonsStack.distribution = .equalSpacing

        encryptButton.setTitle(L10n.encrypt, for: .normal)
        encryptButton.addTarget(self, action: #selector(encryptTapped), for: .touchUpInside)

        decryptButton.setTitle(L10n.decrypt, for: .normal)
        decryptButton.addTarget(self, action: #selector(decryptTapped), for: .touchUpInside)

        resetButton.setTitle(L10n.reset, for: .normal)
        resetButton.addTarget(self, action: #selector(resetTapped), for: .touchUpInside)
    }

    private func layoutLabelsAndFields() {
        stackView.addArrangedSubview(messageLabel)
        messageLabel.pinEdges(toSuperviewEdges: [.left, .right], insets: .horizontal(15))

        stackView.addArrangedSubview(messageField)
        messageField.pinEdges(toSuperviewEdges: [.left, .right], insets: .horizontal(15))

        stackView.setCustomSpacing(30, after: messageField)
    }

    private func layoutButtons() {
        stackView.addArrangedSubview(buttonsStack)
        buttonsStack.pinEdges(toSuperviewEdges: [.left, .right], insets: .horizontal(15))

        buttonsStack.addArrangedSubview(encryptButton)
        buttonsStack.addArrangedSubview(decryptButton)
        buttonsStack.addArrangedSubview(resetButton)

        stackView.setCustomSpacing(30, after: resetButton)
    }

    @objc private func keyboardWillHide() {
        viewModel.updatedContent(message: messageField.text ?? "")
    }

    @objc private func encryptTapped() {
        viewModel.startEncrypting()
    }

    @objc private func decryptTapped() {
        viewModel.startDecrypting()
    }

    @objc private func resetTapped() {
        viewModel.resetEncryption()
    }
}

extension MessageStorageController: MessageStorageViewModelObserver {
    func updated(with state: MessageStorageState) {
        encryptButton.isEnabled =
            !state.isEncrypted
                && state.message.count > 0

        decryptButton.isEnabled = state.isEncrypted
        resetButton.isEnabled = state.isEncrypted || state.pubKey != nil

        publicKeyValueLabel.text = state.pubKey
        encryptedDataLabel.text = state.encryptedMessage
        decryptedDataLabel.text = state.decryptedMessage
    }

    private func showError(text: String) {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: L10n.ok, style: .default) { [weak self] _ in
            self?.viewModel.dismissedError()
        }
        alert.addAction(okAction)

        present(alert, animated: true)
    }
}

extension MessageStorageController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
