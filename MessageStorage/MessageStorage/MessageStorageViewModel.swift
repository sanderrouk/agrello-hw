import ProjectCrypto
import ProjectData

protocol MessageStorageViewModelObserver {
    func updated(with state: MessageStorageState)
}

final class MessageStorageViewModel {
    private var state = MessageStorageState()
    private var observers = WeakObjects<MessageStorageViewModelObserver>()

    private let secureEnclave: SecureEnclave

    init(secureEnclave: SecureEnclave) {
        self.secureEnclave = secureEnclave
        if let pubKey = try? secureEnclave.fetchPublicKey() {
            update(with: .generatedNewPublicKey(pubKey))
        }
    }

    func subscribe(_ observer: MessageStorageViewModelObserver) {
        observers.append(observer)
        observer.updated(with: state)
    }

    func unsubscribe(_ observer: MessageStorageViewModelObserver) {
        observers.remove(observer)
    }

    func updatedContent(message: String) {
        update(with: .updatedMessageAndPassword(message))
    }

    func startEncrypting() {
        do {
            generateNewKeypair()
            let data = state.message.data(using: .utf8) ?? Data()
            let encryptedData = try secureEnclave.encrypt(data: data)
            let stringData = encryptedData.map { String(format: "%02hhx", $0) }.joined()
            update(with: .finishedEncrypting(stringData, encryptedData))
        } catch {
            update(with: .producedError(error.localizedDescription))
        }
    }

    func startDecrypting() {
        guard let data = state.encryptedData else { return }
        do {
            let decryptedData = try secureEnclave.decrypt(data: data)
            let stringMessage = String(data: decryptedData, encoding: .utf8)
            update(with: .finishedDecrypting(stringMessage ?? ""))
        } catch {
            update(with: .producedError(error.localizedDescription))
        }
    }

    func resetEncryption() {
        secureEnclave.clearKeys()
        update(with: .finishedResettingEncryption)
    }

    func dismissedError() {
        update(with: .dismissedError)
    }

    private func generateNewKeypair() {
        do {
            try secureEnclave.generateKeyPair()
            let pubKey = try secureEnclave.fetchPublicKey()
            update(with: .generatedNewPublicKey(pubKey))
        } catch {
            update(with: .producedError(error.localizedDescription))
        }
    }

    private func update(with action: Action) {
        state = MessageStorageViewModel.reduce(previous: state, action: action)
        observers.forEach { $0.updated(with: state) }
    }
}
