import MessageStorage
import ProjectCrypto
import UIKit

struct RootFactory {
    func make(window: UIWindow) -> RootCoordinator {
        #if DEV
            let identifier = "io.rouk.ExampleApp-dev"
        #else
            let identifier = "io.rouk.ExampleApp"
        #endif

        let secureEncalve = SecureEnclaveImplementation(organizationIdentifier: identifier)
        let messageStorageFactory = MessageStorageFactoryImplementation(secureEnclave: secureEncalve)
        return RootCoordinator(window: window, messageStorageFactory: messageStorageFactory)
    }
}
