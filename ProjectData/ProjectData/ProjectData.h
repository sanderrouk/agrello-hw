//
//  ProjectData.h
//  ProjectData
//
//  Created by Sander Rõuk on 08/06/2020.
//  Copyright © 2020 Rouk OÜ. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ProjectData.
FOUNDATION_EXPORT double ProjectDataVersionNumber;

//! Project version string for ProjectData.
FOUNDATION_EXPORT const unsigned char ProjectDataVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ProjectData/PublicHeader.h>


