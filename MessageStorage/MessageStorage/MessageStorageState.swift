struct MessageStorageState {
    fileprivate(set) var message = ""
    fileprivate(set) var encryptedMessage = ""
    fileprivate(set) var encryptedData: Data?
    fileprivate(set) var decryptedMessage = ""
    fileprivate(set) var isEncrypted = false
    fileprivate(set) var pubKey: String?
    fileprivate(set) var errorMessage: String?
}

extension MessageStorageViewModel {
    enum Action {
        case updatedMessageAndPassword(String)
        case finishedEncrypting(String, Data)
        case finishedDecrypting(String)
        case finishedResettingEncryption
        case generatedNewPublicKey(String)
        case producedError(String)
        case dismissedError
    }

    static func reduce(previous: MessageStorageState, action: Action) -> MessageStorageState {
        var state = previous

        switch action {
        case let .updatedMessageAndPassword(message):
            state.message = message

        case let .finishedEncrypting(message, data):
            state.isEncrypted = true
            state.encryptedMessage = message
            state.encryptedData = data

        case .finishedResettingEncryption:
            state.isEncrypted = false
            state.pubKey = nil
            state.decryptedMessage = ""
            state.encryptedData = nil
            state.encryptedMessage = ""

        case let .finishedDecrypting(message):
            state.decryptedMessage = message

        case let .generatedNewPublicKey(stringKey):
            state.pubKey = stringKey

        case let .producedError(errorMessage):
            state.errorMessage = errorMessage

        case .dismissedError:
            state.errorMessage = nil
        }

        return state
    }
}
