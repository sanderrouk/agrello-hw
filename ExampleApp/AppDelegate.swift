import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    private var rootFactory: RootFactory
    private var rootCoordinator: RootCoordinator?

    override init() {
        self.rootFactory = RootFactory()
        super.init()
    }

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        let window = UIWindow()
        self.window = window

        let coordinator = rootFactory.make(window: window)
        self.rootCoordinator = coordinator
        coordinator.start()

        return true
    }
}
