import UIKit

public protocol Coordinator {
    func start() -> UIViewController
}
