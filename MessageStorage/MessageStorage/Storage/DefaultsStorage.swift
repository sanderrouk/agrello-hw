import ProjectData

struct DefaultsStorage {
    @UserDefault("kEncryptedMessage", defaultValue: "")
    var message: String

    @UserDefault("kEncryptedPassword", defaultValue: "")
    var password: String
}
