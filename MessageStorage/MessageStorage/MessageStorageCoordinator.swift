import ProjectUI
import UIKit

final class MessageStorageCoordinator: Coordinator {
    private let navigationController: UINavigationController
    private let makeContentViewController: () -> UIViewController

    init(
        navigationController: UINavigationController,
        makeContentViewController: @escaping () -> UIViewController
    ) {
        self.navigationController = navigationController
        self.makeContentViewController = makeContentViewController
    }

    func start() -> UIViewController {
        let viewController = makeContentViewController()
        navigationController.setViewControllers([viewController], animated: false)

        return navigationController
    }
}
