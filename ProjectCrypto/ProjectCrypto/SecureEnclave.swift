import Foundation
import LocalAuthentication
import Resources
import Security

public protocol SecureEnclave {
    typealias KeyPair = (public: SecKey, private: SecKey)
    @discardableResult
    func generateKeyPair() throws -> KeyPair
    func fetchPublicKey() throws -> String
    func clearKeys()
    func encrypt(data: Data) throws -> Data
    func decrypt(data: Data) throws -> Data
}

public enum SecureEnclaveError: Error {
    case failedToGenerateKeys
    case failedToEstablishAccessControl
    case failedToStorePublicKey
    case failedToFetchKey
    case failedToEncrypt
    case failedToDecrypt

    public var localizedDescription: String {
        switch self {
        case .failedToGenerateKeys:
            return L10n.failedToGenerateKeys

        case .failedToEstablishAccessControl:
            return L10n.failedToEstablishAccessControl

        case .failedToStorePublicKey:
            return L10n.failedToStorePublicKey

        case .failedToFetchKey:
            return L10n.failedToFetchKey

        case .failedToEncrypt:
            return L10n.failedToEncrypt

        case .failedToDecrypt:
            return L10n.failedToDecrypt
        }
    }
}

// swiftlint:disable force_cast
public final class SecureEnclaveImplementation: SecureEnclave {
    private let organizationIdentifier: String
    private let organizationIdentifierPrivateKey: String

    public init(organizationIdentifier: String) {
        self.organizationIdentifier = organizationIdentifier
        self.organizationIdentifierPrivateKey = "\(organizationIdentifier).private"
    }

    @discardableResult
    public func generateKeyPair() throws -> KeyPair {
        let accessControl = try fetchAccessControl()

        let privateKeyParams: [String: Any] = [
            kSecAttrLabel as String: organizationIdentifierPrivateKey,
            kSecAttrIsPermanent as String: true,
            kSecAttrAccessControl as String: accessControl
        ]

        var params: [String: Any] = [
            kSecAttrKeyType as String: kSecAttrKeyTypeECSECPrimeRandom as String,
            kSecAttrKeySizeInBits as String: 256,
            kSecPrivateKeyAttrs as String: privateKeyParams
        ]

        // The secure enclave can't be accessed on simulators, will switch over to keychain instead
        #if !targetEnvironment(simulator)
            params[kSecAttrTokenID as String] = kSecAttrTokenIDSecureEnclave
        #endif

        var publicKey, privateKey: SecKey?

        let status = SecKeyGeneratePair(params as CFDictionary, &publicKey, &privateKey)
        guard status == errSecSuccess,
            let pubKey = publicKey, let privKey = privateKey else { throw SecureEnclaveError.failedToGenerateKeys }

        try storePublicKey(pubKey)

        return (pubKey, privKey)
    }

    public func fetchPublicKey() throws -> String {
        let envelope = try fetchPublicKeyEnvelope()
        guard let data = envelope[kSecValueData as String] as? Data else {
            throw SecureEnclaveError.failedToFetchKey
        }

        return data.map { String(format: "%02hhx", $0) }.joined()
    }

    public func clearKeys() {
        let query: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrKeyType as String: kSecAttrKeyTypeECSECPrimeRandom as String,
            kSecAttrKeyClass as String: kSecAttrKeyClassPublic,
            kSecAttrApplicationTag as String: organizationIdentifier
        ]

        let privateQuery: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrKeyClass as String: kSecAttrKeyClassPrivate,
            kSecAttrLabel as String: organizationIdentifierPrivateKey,
            kSecReturnRef as String: true
        ]

        SecItemDelete(query as CFDictionary)
        SecItemDelete(privateQuery as CFDictionary)
    }

    public func encrypt(data: Data) throws -> Data {
        let publicKey = try fetchPublicSecKey()
        let result = SecKeyCreateEncryptedData(
            publicKey,
            SecKeyAlgorithm.eciesEncryptionStandardX963SHA256AESGCM,
            data as CFData,
            nil
        )

        guard let foundResult = result else {
            throw SecureEnclaveError.failedToEncrypt
        }

        return foundResult as Data
    }

    public func decrypt(data: Data) throws -> Data {
        let privateKey = try fetchPrivateSecKey()

        let result = SecKeyCreateDecryptedData(
            privateKey,
            SecKeyAlgorithm.eciesEncryptionStandardX963SHA256AESGCM,
            data as CFData,
            nil
        )

        guard let foundResult = result else { throw SecureEnclaveError.failedToDecrypt }

        return foundResult as Data
    }

    private func storePublicKey(_ secKey: SecKey) throws {
        let context = LAContext()

        let query: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrKeyType as String: kSecAttrKeyTypeECSECPrimeRandom as String,
            kSecAttrKeyClass as String: kSecAttrKeyClassPublic,
            kSecAttrApplicationTag as String: organizationIdentifier,
            kSecValueRef as String: secKey,
            kSecAttrIsPermanent as String: true,
            kSecUseAuthenticationContext as String: context,
            kSecReturnData as String: true
        ]

        var raw: CFTypeRef?
        var status = SecItemAdd(query as CFDictionary, &raw)

        if status == errSecDuplicateItem {
            status = SecItemDelete(query as CFDictionary)
            status = SecItemAdd(query as CFDictionary, &raw)
        }

        guard status == errSecSuccess else {
            throw SecureEnclaveError.failedToStorePublicKey
        }
    }

    private func fetchAccessControl() throws -> SecAccessControl {
        var flags: SecAccessControlCreateFlags = [.userPresence, .privateKeyUsage, .applicationPassword]
        if #available(iOS 11.3, *) {
            flags = [.biometryAny, .privateKeyUsage, .applicationPassword]
        }

        let accessControl = SecAccessControlCreateWithFlags(
            kCFAllocatorDefault,
            kSecAttrAccessibleWhenPasscodeSetThisDeviceOnly,
            flags,
            nil
        )

        guard let access = accessControl else {
            throw SecureEnclaveError.failedToEstablishAccessControl
        }

        return access
    }

    private func fetchPublicSecKey() throws -> SecKey {
        let envelope = try fetchPublicKeyEnvelope()
        return envelope[kSecValueRef as String] as! SecKey
    }

    private func fetchPublicKeyEnvelope() throws -> [String: Any] {
        let query: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrKeyType as String: kSecAttrKeyTypeECSECPrimeRandom as String,
            kSecAttrApplicationTag as String: organizationIdentifier,
            kSecAttrKeyClass as String: kSecAttrKeyClassPublic,
            kSecReturnData as String: true,
            kSecReturnRef as String: true,
            kSecReturnPersistentRef as String: true
        ]

        let reference = try fetchKey(query)

        let dictionary = reference as! CFDictionary
        guard let envelope = dictionary as? [String: Any] else { throw SecureEnclaveError.failedToFetchKey }
        return envelope
    }

    private func fetchPrivateSecKey() throws -> SecKey {
        let query: [String: Any] = [
            kSecClass as String: kSecClassKey,
            kSecAttrKeyClass as String: kSecAttrKeyClassPrivate,
            kSecAttrLabel as String: organizationIdentifierPrivateKey,
            kSecReturnRef as String: true,
            kSecUseOperationPrompt as String: L10n.privateKeyPrompt
        ]

        let ref = try fetchKey(query)
        return ref as! SecKey
    }

    private func fetchKey(_ query: [String: Any]) throws -> CFTypeRef {
        var result: CFTypeRef?
        let status = SecItemCopyMatching(query as CFDictionary, &result)

        guard status == errSecSuccess,
            let foundResult = result else {
            throw SecureEnclaveError.failedToFetchKey
        }

        return foundResult
    }
}

// swiftlint:enable force_cast
