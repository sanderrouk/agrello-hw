// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
public enum L10n {
    /// Example App
    public static let appTitle = L10n.tr("Localizable", "appTitle")
    /// Decrypt
    public static let decrypt = L10n.tr("Localizable", "decrypt")
    /// Decrypted data
    public static let decryptedData = L10n.tr("Localizable", "decryptedData")
    /// Done
    public static let done = L10n.tr("Localizable", "done")
    /// Enable biometrics
    public static let enableBiometrics = L10n.tr("Localizable", "enableBiometrics")
    /// Encrypt
    public static let encrypt = L10n.tr("Localizable", "encrypt")
    /// Encrypted data (Hex)
    public static let encryptedData = L10n.tr("Localizable", "encryptedData")
    /// Enter message
    public static let enterMessage = L10n.tr("Localizable", "enterMessage")
    /// Enter password
    public static let enterPassword = L10n.tr("Localizable", "enterPassword")
    /// Failed to decrypt message.
    public static let failedToDecrypt = L10n.tr("Localizable", "failedToDecrypt")
    /// Failed to encrypt data
    public static let failedToEncrypt = L10n.tr("Localizable", "failedToEncrypt")
    /// Something went wrong when establishing access control.
    public static let failedToEstablishAccessControl = L10n.tr("Localizable", "failedToEstablishAccessControl")
    /// Failed to retrieve the key.
    public static let failedToFetchKey = L10n.tr("Localizable", "failedToFetchKey")
    /// The secure enclave failed to generate a new keypair.
    public static let failedToGenerateKeys = L10n.tr("Localizable", "failedToGenerateKeys")
    /// Something went wrong while trying to store the public key.
    public static let failedToStorePublicKey = L10n.tr("Localizable", "failedToStorePublicKey")
    /// Generate new keypair
    public static let generateNewKeypair = L10n.tr("Localizable", "generateNewKeypair")
    /// Message
    public static let messagePlaceholder = L10n.tr("Localizable", "messagePlaceholder")
    /// Ok
    public static let ok = L10n.tr("Localizable", "ok")
    /// Password
    public static let passwordPlaceholder = L10n.tr("Localizable", "passwordPlaceholder")
    /// Trying to access the private key on the device.
    public static let privateKeyPrompt = L10n.tr("Localizable", "privateKeyPrompt")
    /// Public key
    public static let publicKey = L10n.tr("Localizable", "publicKey")
    /// Reset
    public static let reset = L10n.tr("Localizable", "reset")
    /// Use biometrics
    public static let useBiometrics = L10n.tr("Localizable", "useBiometrics")
    /// Use password
    public static let usePassword = L10n.tr("Localizable", "usePassword")
}

// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
    private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
        // swiftlint:disable:next nslocalizedstring_key
        let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
        return String(format: format, locale: Locale.current, arguments: args)
    }
}

private final class BundleToken {}
