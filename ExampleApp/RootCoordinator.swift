import MessageStorage
import ProjectUI
import UIKit

final class RootCoordinator {
    private let window: UIWindow
    private let messageStorageFactory: MessageStorageFactory

    private var child: Coordinator?

    init(window: UIWindow, messageStorageFactory: MessageStorageFactory) {
        self.window = window
        self.messageStorageFactory = messageStorageFactory
    }

    func start() {
        let coordinator = messageStorageFactory.make()
        child = coordinator
        window.rootViewController = coordinator.start()
        window.makeKeyAndVisible()
    }
}
