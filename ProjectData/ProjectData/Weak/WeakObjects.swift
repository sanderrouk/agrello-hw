public class WeakObjects<Element> {
    private var elements: [Weak<Element>] = []

    public required init() {}

    public func append(_ element: Element) {
        if elements.contains(Weak(element)) { return }
        elements.append(Weak(element))
        release()
    }

    public func remove(_ element: Element) {
        release()
        guard let index = elements.firstIndex(where: { $0 == Weak(element) }) else { return }
        elements.remove(at: index)
    }

    public func release() {
        elements = elements.compactMap { $0.element == nil ? nil : $0 }
    }
}

extension WeakObjects: Sequence {
    public var count: Int { return elements.count }

    public func makeIterator() -> AnyIterator<Element> {
        var generator = elements.makeIterator()

        return AnyIterator {
            var next: Weak<Element>?
            var weakObjectElement: Element?
            repeat {
                next = generator.next()
                weakObjectElement = next?.element as? Element
            } while next != nil && weakObjectElement == nil

            return weakObjectElement
        }
    }
}
