import Foundation

public struct Weak<T>: Equatable {
    public weak var element: AnyObject?

    public init(_ object: T) {
        element = object as AnyObject
    }

    public static func == <T>(lhs: Weak<T>, rhs: Weak<T>) -> Bool {
        return lhs.element === rhs.element
    }
}
